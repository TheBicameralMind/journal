import React, { Component } from 'react'; 
import { Form, Segment } from 'semantic-ui-react';

class TitleBar extends Component {
    constructor(props) {
        super(props); 

        let entries = this.createCollectionMenuOptions(props.Data.collections);

        this.state = {
            title: props.Data.entry_title, 
            created: props.Data.entry_created, 
            modified: props.Data.entry_modified, 
            collection: props.Data.entry_collection, 
            collections: entries, 
        };

        this.method_dataChanged = props.onChange; 
        this.method_submit = props.dataSubmit; 

    }

    createCollectionMenuOptions = (names) => {
        return names.map(data => {
            return {
                key: data.collectionTitle,
                value: data.collectionTitle,
                text: data.collectionTitle
            };
        });
    }

    onChange = (e, { name, value }) => this.method_dataChanged(name, value); 

    render() {
        return (
            <Form onSubmit={this.method_submit}>
                <Form.Group>
                    <Form.Input  width={8} fluid defaultValue={this.state.title}      name="title"      label="Title"        onChange={this.onChange}/>
                    <Form.Select width={3} fluid defaultValue={this.state.collection} name="collection" label="Collection"   onChange={this.onChange} options={this.state.collections}/>
                    <Form.Input  width={2} fluid value={this.state.created}                             label="Date Created" readOnly />
                    <Form.Input  width={2} fluid value={this.state.modified}          name="modified"   label="Date Modified" readOnly />
                    <Form.Button width={1} fluid                                                         label="Save Changes" type="submit" icon="save" />
                </Form.Group>
            </Form>
        );
    }
}

export default TitleBar; 