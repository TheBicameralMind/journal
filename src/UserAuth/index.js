import Login from './Login'; 
import Register from './Register'; 
import DataLoading from './DataLoading'; 

export {
    Login, 
    Register, 
    DataLoading
}; 