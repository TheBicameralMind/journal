import './UserAuth.css'; 
import React, { Component } from 'react'; 
import { Redirect } from 'react-router-dom'; 
import { Input, Message, Icon, Button, Transition, Form, Segment, Grid, Header } from 'semantic-ui-react'; 

class Login extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            loggedIn: false, 
            needsRegister: false, 
            currentPassword: "", 
            visible: false
        }
    }

    login = () => {
        if (this.state.currentPassword === "") return;
        window.ipcRenderer.invoke('login-attempt', this.state.currentPassword).then((valid) => {
            if (valid === true) {
                this.setState({ loggedIn: true }); 
            } else {
                this.setState({ visible: true }); 

                setTimeout(() => {
                    this.setState({ visible: false }); 
                }, 2000); 
            }
        }); 
    }

    changeHandler = (event) => {
        this.setState({ currentPassword: event.target.value }); 
    }
    
    render() {
        if (this.state.loggedIn === true) {
            return (
                <Redirect to='/home'/>
            ); 
        }

        if (this.state.needsRegister === true) {
            return (
                <Redirect to='/register' />
            )
        }

        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign="middle">
                <Grid.Column style={{ maxWidth: '30%' }}>
                    <Form size="large">
                        <Segment basic>
                            <Header size="huge" disabled content="Welcome back." />
                            <Form.Input icon="lock" iconPosition="left" type="password" placeholder="Password" onChange={this.changeHandler} error={this.state.visible}/>
                            <Button color="green" fluid size="large" onClick={this.login} content="Login"/>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }
}

export default Login; 