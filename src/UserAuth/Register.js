import './UserAuth.css'; 
import { Redirect } from 'react-router-dom';
import React, { Component, Fragment } from 'react'; 
import { Button, Input, Popup, Transition, Message, Grid, Form, Segment, Header } from 'semantic-ui-react'; 

class Register extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            newPassword: null, 
            confirmNewPassword: null,
            visible: false, 
            shouldLoad: false
        }

        window.ipcRenderer.once('set-password', (event, set) => {
            this.setState({ shouldLoad: true }); 
        }); 
    }

    setPassword = (event) => {
        event.preventDefault(); 
        
        if (this.state.newPassword === this.state.confirmNewPassword && this.state.newPassword !== "" && this.state.confirmNewPassword !== "") {         
            window.ipcRenderer.send("set-password", this.state.newPassword); 

        } else {
            this.setState({ visible: true }); 

            setTimeout(() => {
                this.setState({ visible: false }); 
            }, 2000); 
        }
    }

    newPassHandler = (event) => {
        event.preventDefault();
        this.setState({ newPassword: event.target.value });
    }

    confirmPassHandler = (event) => {
        event.preventDefault(); 
        this.setState({ confirmNewPassword: event.target.value }); 
    }

    render() {
        if (this.state.shouldLoad === true) {
            return (
                <Redirect to='/' />
            ); 
        }

        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign="middle">
                <Grid.Column style={{ maxWidth: '30%' }}>
                    <Form size="large">
                        <Segment basic>
                            <Header size="huge" disabled content="Welcome." />
                            <Form.Input icon="lock" iconPosition="left" type="password" placeholder="New password" onChange={this.newPassHandler} error={this.state.visible}/>
                            <Form.Input icon="lock" iconPosition="left" type="password" placeholder="Confirm password" onChange={this.confirmPassHandler} error={this.state.visible}/>
                            <Button color="green" fluid size="large" onClick={this.setPassword} content="Register"/>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        ); 
    }
}

export default Register; 