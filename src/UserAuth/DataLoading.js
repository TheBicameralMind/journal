import './UserAuth.css'; 
import React, { Component } from 'react'; 
import { Redirect } from 'react-router-dom';
import { Loader } from 'semantic-ui-react'; 

class DataLoading extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            proceedToLogin: false,
            proceedToRegister: false
        }; 

        window.ipcRenderer.once('should-login', (event, login) => {
            if (login === true) {
                this.setState({ proceedToLogin: true }); 
            } else {
                this.setState({ proceedToRegister: true }); 
            }
        }); 
    }

    componentDidMount = () => {
        window.ipcRenderer.send('should-login'); 
    }

    render() {
        if (this.state.proceedToRegister === true) {
            return (
                <Redirect to='/register' />
            ); 
        }

        if (this.state.proceedToLogin === true) {
            return (
                <Redirect to='/login' />
            ); 
        }

        return (
            <Loader active content="Loading..." />
        )
    }
}

export default DataLoading; 