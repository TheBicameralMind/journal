import './index.css';
import App from './Main/App';
import React from 'react';
import ReactDOM from 'react-dom';
// import Login from './UserAuth/Login'; 
import 'semantic-ui-css/semantic.min.css'
// import DataLoading from './UserAuth/DataLoading'; 
// import Register from './UserAuth/Register'; 
import * as serviceWorker from './serviceWorker';
import { HashRouter, Route } from 'react-router-dom'; 
import { Login, Register, DataLoading } from './UserAuth'; 

ReactDOM.render(
	<React.StrictMode>
		<HashRouter>
			<div>
				<Route path="/" exact   component={ App } />
				<Route path="/login"    component={ Login } />
				<Route path="/home"	    component={ App } />
				<Route path="/register" component={ Register } />
			</div>
		</HashRouter>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
