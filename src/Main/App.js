import React, { Component } from 'react';
import { Sidebar, Segment, Breadcrumb } from 'semantic-ui-react'; 
import MainEditor from '../MainEditor'; 
import TitleBar from '../TitleBar';
import { EntryMenu } from '../Menus';  
import isEqual from 'lodash.isequal'; 
import './App.css';

class App extends Component {
	constructor(props) {
		super(props); 

		this.state = {
			entry_id: "",
			entry_title: "", 
			entry_created: "", 
			entry_content: "", 
			entry_modified: "",
			entry_collection: "",
			collections: [], 
		}; 

		this.newInfo = {}; 

		window.ipcRenderer.on("get-content-for-entry", (event, editorContent) => {
			try {
				this.setState({ 
					entry_content: editorContent.content, 
					entry_title: editorContent.title, 
					entry_created: editorContent.created, 
					entry_modified: editorContent.modified, 
					entry_collection: editorContent.collection, 
				}); 
			} catch (err) {
				this.setState({ 
					entry_content: "",
					entry_title: "",
					entry_created: "",
					entry_modified: "",
					entry_collection: ""
				}); 
			}

			this.newInfo = {...this.state}; 
		}); 

		window.ipcRenderer.on("get-collections", (event, collectionNames) => {
			this.setState({ collections: collectionNames }); 
		}); 
	}

	componentDidMount = () => {
		window.ipcRenderer.send('get-collections'); 
	}

	getDate = () => {
		let date = new Date(); 
		return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`; 
	}

	setEditorContent = (id) => {
		window.ipcRenderer.send("get-content-for-entry", id); 
		this.setState({ entry_id: id }); 
	}

	subCompOnChange = (key, value) => {
		key = `entry_${key}`; 
		this.newInfo = {...this.newInfo, [key]: value}; 
	} 

	submit = () => {
		if (isEqual(this.state, this.newInfo)) {
			return; 
		}
		this.newInfo.entry_modified = this.getDate(); 
		this.setState(this.newInfo);
		window.ipcRenderer.send('update-entry', this.newInfo);
	}
	
	render() {
		return (
			<Sidebar.Pushable as={Segment} basic>
				<Sidebar className="mySidebar" visible>
					<EntryMenu setContent={this.setEditorContent} />
				</Sidebar>
				<Sidebar.Pusher className="myEditor"> 
				<Segment basic attached="top">
					<TitleBar key={this.state.entry_title + this.state.entry_modified} Data={this.state} onChange={this.subCompOnChange} dataSubmit={this.submit}/>
				</Segment>
					<MainEditor key={this.state.entry_content} content={this.state.entry_content} onChange={this.subCompOnChange}/>
				</Sidebar.Pusher>
			</Sidebar.Pushable>
		);
	}
}

export default App;
