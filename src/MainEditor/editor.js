import React, { Component } from 'react'; 
import CodeMirror from '@skidding/react-codemirror'; 

require('codemirror/lib/codemirror.css'); 
require('codemirror/mode/markdown/markdown'); 
require('codemirror/mode/gfm/gfm')
require('codemirror/mode/python/python'); 
require('codemirror/theme/nord.css'); 

class Editor extends Component {
    constructor(props) {
        super(props); 
    }

    updateCode = (e) => {
        this.props.onChange(e); 
    }

    render() {
        var options = {
            mode: 'gfm', 
            theme: 'nord', 
            lineWrapping: true
        }

        return (
            <CodeMirror value={this.props.value} onChange={this.updateCode} options={options} height="100%" />
        ); 
    }
}

export default Editor; 