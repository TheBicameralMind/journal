import React, { Component } from 'react'; 
import ReactMarkdown from 'react-markdown'; 
import Editor from './editor.js'; 
import SplitPane from 'react-split-pane';
import 'github-markdown-css';
import './MainEditor.css'; 


class MainEditor extends Component {
	constructor(props) {
		super(props); 

		this.state = { markdownSrc: props.content }; 

		this.contentChanged = props.onChange; 
	}

	onMarkdownChange = (md) => {
		this.setState({ markdownSrc: md });
		this.contentChanged("content", md); 
	}
	
	render() { // TODO new markdown renderer perhaps? 
		return (
			<SplitPane className="mySplitPane" split="vertical" defaultSize="50%">
				<Editor className="editor" value={this.state.markdownSrc} onChange={this.onMarkdownChange} />
				<ReactMarkdown className="result markdown-body" source={this.state.markdownSrc} />
			</SplitPane>
		)
	}
}

export default MainEditor; 