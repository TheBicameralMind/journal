import React, { Component, Fragment } from 'react'; 
import { Menu, Label, Button, Popup, Breadcrumb, Segment, Input, List } from 'semantic-ui-react'; 
import { CreateEntryModal, CreateCollectionModal } from '../Modals';
import EntryMenuItem from './EntryMenuItem'; 

class EntryMenu extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            menu_collectionItems: [], 
            menu_entryItems: [], 
            menu_showCollections: true,

            method_setContent: props.setContent, 

            currentCollection: ""
        }; 

        window.ipcRenderer.on('get-collection-names-and-dates', (event, collectionData) => {
            this.createCollectionMenuItems(collectionData); 
        }); 

        window.ipcRenderer.on('get-entries-for-collection', (event, entryData) => {
            this.createEntryMenuItems(entryData); 
        }); 
    }

    componentDidMount = () => {
        this.refreshCollectionList(); 
    }
	
	menuClick = (name, type, id ) => {
        if (type === "collection") {
            this.refreshEntryList(name); 
        } else {
            this.state.method_setContent(id); 
        }
    }

    refreshCollectionList = () => window.ipcRenderer.send('get-collection-names-and-dates'); 

    refreshEntryList = (name) => {
        window.ipcRenderer.send('get-entries-for-collection', name);
        this.setState({ currentCollection: name }); 
    }

    backButton = (event) => {
        this.refreshCollectionList(); 
        this.setState({ menu_showCollections: true});
    }

    createCollectionMenuItems = (collectionData) => {
        let tmpPanels = []; 
        for (let i = 0; i < collectionData.length; i++) {
            let data = collectionData[i]; 
            data.type = "collection"; 
            let tmp = {
                key: data.collectionTitle,
                name: data.collectionTitle,
                content: <EntryMenuItem data={data} menuClick={this.menuClick} refresh={this.refreshCollectionList} />
            }

            tmpPanels.push(tmp); 
        }
        this.setState({ menu_collectionItems: tmpPanels }); 
    }

    createEntryMenuItems = (entryData) => {
        let tmpEntries = []; 
        for (let i = 0; i < entryData.length; i++) {
            let data = entryData[i];
            data.type = "entry" 
            let tmp = {
                key: data.title, 
                name: data.title, 
                content: <EntryMenuItem type="entry" data={data} menuClick={this.menuClick} refresh={this.refreshEntryList} />,
            };
            
            tmpEntries.push(tmp); 
        }
        this.setState({ menu_entryItems: tmpEntries, menu_showCollections: false }); 
    }

    render() {
        let button = this.state.menu_showCollections ? <CreateCollectionModal refresh={this.refreshCollectionList} /> :
            <Button.Group fluid>
                <Button basic secondary icon="left arrow" onClick={this.backButton} />
                <CreateEntryModal collection={this.state.currentCollection} key={this.state.currentCollection} refresh={this.refreshEntryList} />
            </Button.Group>;

        return (
            <Fragment>
                <Segment.Group raised>
                    <Segment content={button} />
                    <Segment>
                        <Input placeholder={`Search ${this.state.menu_showCollections ? "Collections" : "Entries"}`} icon='search' />
                    </Segment>
                </Segment.Group>
                <List selection relaxed items={this.state.menu_showCollections ? this.state.menu_collectionItems : this.state.menu_entryItems} />
            </Fragment>
        )
    }
}

export default EntryMenu; 