import React, { Component } from 'react'; 
import { Dropdown } from 'semantic-ui-react'; 

import faker from 'faker'; 

class TagMenu extends Component {
    constructor(props) {
        super(props);
         
        const stateOptions = faker.definitions.address.state.map((state) => ({
            key: state, 
            text: state, 
            value: state
        }));
        
        this.state = {
            options: stateOptions, 
        }; 
    }

    handleChange = (e, { value }) => this.setState({ currentValues: value }); 

    handleAddition = (e, { value }) => {
        this.setState((prevState) => ({
            options: [{ text: value, value }, ...prevState.options],
        }));
    }

    render() {
        const { currentValues } = this.state; 

        return (
            <Dropdown options={this.state.options} placeholder="Select tags" value={currentValues} 
                onChange={this.handleChange} onAddItem={this.handleAddition} 
                search selection fluid multiple allowAdditions />
        ); 
    }
}

export default TagMenu; 