import EntryMenu from './EntryMenu'; 
import TagMenu from './TagMenu'; 

export {
    EntryMenu, 
    TagMenu
}; 