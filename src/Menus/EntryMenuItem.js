import React, { Component, Fragment } from 'react'; 
import { Label, Popup, List, Icon } from 'semantic-ui-react';
import { EntryDetailModal, CollectionDetailModal } from '../Modals'; 

class EntryMenuItem extends Component {
    constructor(props) {
        super(props); 
    }

    onClick = () => this.props.menuClick(this.props.data.collectionTitle, this.props.data.type, this.props.data.id); 

    render() {
        if (this.props.type === 'entry') {
            return (
                <List.Item>
                    <List.Content floated='left' verticalAlign="middle" content={<List.Icon size="large" name="book" onClick={this.onClick} />} />
                    <List.Content floated='right' verticalAlign="middle" content={<EntryDetailModal data={this.props.data} refresh={this.props.refresh} />} />
                    <List.Content>
                        <List.Header as='a' content={this.props.data.title} onClick={this.onClick} /> 
                        <List.Description content={this.props.data.created} />
                    </List.Content>
                </List.Item>
            ); 
        }

        if (this.props.data.min === null && this.props.data.max === null) {
            return (
                <List.Item>
                    <List.Content floated='left' verticalAlign="middle" content={ <Label size="medium" content={this.props.data.total} onClick={this.onClick} />} />
                    <List.Content floated='right' verticalAlign="middle" content={ <CollectionDetailModal title={this.props.data.collectionTitle} refresh={this.props.refresh}/>} />
                    <List.Content>
                        <List.Header as='a' content={this.props.data.collectionTitle} onClick={this.onClick} />
                        <List.Description content="No entries" />
                    </List.Content>
                </List.Item>
            );
        }

        return (
            <List.Item>
                <List.Content floated='left' verticalAlign="middle" content={<Label size="medium" content={this.props.data.total} onClick={this.onClick} />} />
                <List.Content floated='right' verticalAlign="middle" content={<CollectionDetailModal title={this.props.data.collectionTitle} refresh={this.props.refresh} />} />
                <List.Content>
                    <List.Header as='a' content={this.props.data.collectionTitle} onClick={this.onClick} />
                    <List.Description content={this.props.data.min === this.props.data.max ? `${this.props.data.min}` : `${this.props.data.min} to ${this.props.data.max}`} />
                </List.Content>
            </List.Item>
        )
    }
}

export default EntryMenuItem; 