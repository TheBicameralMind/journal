import React, { Component } from 'react'; 
import { Modal, Button, Header, Icon, Input, Message, Transition } from 'semantic-ui-react'; 

class CreateEntryModal extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            open: false, 
            loading: false, 
            error: false,
            newCollection: props.collection
        };
        
        this.refresh = props.refresh;
    }

    getDate = () => {
		let date = new Date(); 
		return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`; 
	}

    open = () => this.setState({ open: true, loading: false }); 

    close = () => this.setState({ open: false, loading: false }); 

    createEntry = () => {
        window.ipcRenderer.once('create-entry', (event, result) => {
            this.setState({ error: !result, loading: false }); 
            if (this.state.error !== true) {
                this.close();
                this.refresh(this.state.newCollection); 
            }
        });

        this.setState({ loading: true }); 
        let entryInfo = { 
            entryTitle: this.state.newTitle, 
            entryCollection: this.state.newCollection, 
            entryCreated: this.getDate(), 
            entryModified: this.getDate()
        };
        window.ipcRenderer.send('create-entry', entryInfo); 
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    render() {
        return (
            <Modal trigger={<Button basic primary content="New Entry" onClick={this.open}/>} basic size="small"
                open={this.state.open} closeOnEscape={false} closeOnDimmerClick={false}>
                <Header icon="pencil" content="Create New Entry" />
                <Modal.Content>
                    <Input fluid placeholder="New Entry Name" name="newTitle" onChange={this.handleChange} loading={this.state.loading} error={this.state.error} />
                    <Transition visible={ this.state.error } animation="fade" duratiom={750}>
                        <Message floating error header="Invalid entry name!" content="Cannot be empty."/>
                    </Transition>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='red' inverted onClick={this.close} disabled={this.state.loading}>
                        <Icon name='remove' /> Cancel
                    </Button>
                    <Button color='green' inverted onClick={this.createEntry} disabled={this.state.loading}>
                        <Icon name='checkmark' /> Create
                    </Button>
                </Modal.Actions>
            </Modal>
        ); 
    }
}

export default CreateEntryModal; 