import React, { Component } from 'react'; 
import { Modal, Icon, Header, Button } from 'semantic-ui-react'; 

class EntryDetailModal extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            open: false, 
            loading: false
        };
    }

    open = (event) => this.setState({ open: true });

    close = () => this.setState({ open: false });
    
    delete = () => {
        window.ipcRenderer.once('delete-entry', (event, result) => {
            this.setState({ error: !result, loading: false }); 
            if (this.state.error !== true) {
                this.close(); 
                this.props.refresh(this.props.data.collection); 
            } else {
                console.log(result);
            }
        });

        window.ipcRenderer.send('delete-entry', this.props.data.id); 
        this.setState({ loading: true }); 
    }

    render() {
        return (
            <Modal trigger={ <Icon name="ellipsis horizontal" onClick={this.open}/> } basic open={this.state.open} >
                <Header icon="file alternate outline" size="huge" content={this.props.data.title} />
                {/* <Modal.Content>

                </Modal.Content> */}
                <Modal.Actions>
                    <Button basic color='orange' inverted onClick={this.delete} disabled={this.state.loading}>
                        <Icon name="trash" /> Delete
                    </Button>
                    <Button basic color='red' inverted onClick={this.close} disabled={this.state.loading}>
                        <Icon name='remove' /> Cancel
                    </Button>
                    <Button color='green' inverted onClick={this.createEntry} disabled={this.state.loading}>
                        <Icon name='save outline' /> Save
                    </Button>
                </Modal.Actions>
            </Modal>
        ); 
    }
}

export default EntryDetailModal;