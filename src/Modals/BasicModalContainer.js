import React, { Container } from 'react'; 
import { Modal } from 'semantic-ui-react'; 

class BasicModalContainer extends Component {
    constructor(props) {
        super(props); 
    }

    render() {
        return (
            <Modal trigger={this.props.trigger} open={this.state.open} 
                closeOnEscape={this.props.closeOnlyOnButton} closeOnDimmerClick={this.props.closeOnlyOnButton} >
                
            </Modal>
        ); 
    }
}

export default BasicModalContainer; 