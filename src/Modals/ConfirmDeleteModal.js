import React, { Component } from 'react'; 
import { Modal, Icon, Header, Message, Transition, Button } from 'semantic-ui-react'; 

class ConfirmDeleteModal extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            open: false, 
            loading: false, 
            error: false, 
        }; 

        this.refresh = props.refresh; 
        this.type = props.type; 

        this.header = `Delete ${props.deleting}?`; 
        this.message = `Are you sure you want to delete ${props.deleting}?${props.type === 'collection' ? " It will also delete all contained entries" : ""}`; 
    }

    close = () => this.setState({ open: false, loading: false });

    open = () => this.setState({ open: true, loading: false }); 
    
    delete = () => {
        window.ipcRenderer.once('delete-entry-collection', (event, result) => {
            this.setState({ error: !result, loading: false }); 
            if (this.state.error !== true) {
                this.close(); 
                this.refresh(); 
            }
        }); 

        window.ipcRenderer.send('delete-entry-collection', { type: this.type, title: this.deleting });

    }

    render() {
        return (
            <Modal trigger={<Button fluid basic negative content="Delete" icon="trash alternate outline" onClick={this.open}/>} basic size="small" 
                open={this.state.open} closeOnEscape={false} closeOnDimmerClick={false}>
                    <Header icon="delete" content={this.header} />
                    <Modal.Content>
                        {this.message}
                        <Transition visible={ this.state.error } animation="fade" duratiom={750}>
                            <Message floating error header={`Error deleting ${this.deleting}`} content="Error message goes here in theory" />
                        </Transition>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button basic color='red' inverted onClick={this.close} disabled={this.state.loading}>
                            <Icon name='remove' /> Cancel
                        </Button>
                        <Button color='green' inverted onClick={this.delete} disabled={this.state.loading}>
                            <Icon name='checkmark' /> Delete
                        </Button>
                    </Modal.Actions>
                </Modal>
        ); 
    }
}

export default ConfirmDeleteModal;