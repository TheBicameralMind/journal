import CreateCollectionModal from './CreateCollectionModal'; 
import CreateEntryModal from './CreateEntryModal'; 
import ConfirmDeleteModal from './ConfirmDeleteModal'; 
import EntryDetailModal from './EntryDetailModal'; 
import CollectionDetailModal from './CollectionDetailModal'; 

export {
    CreateCollectionModal, 
    CreateEntryModal, 
    ConfirmDeleteModal, 
    EntryDetailModal, 
    CollectionDetailModal
}; 