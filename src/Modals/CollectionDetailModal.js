import React, { Component } from 'react'; 
import { Modal, Icon, Header, Button } from 'semantic-ui-react';
import { TagMenu } from '../Menus';  

class CollectionDetailModal extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            open: false, 
            loading: false
        }; 
    }

    open = () => this.setState({ open: true });

    close = () => this.setState({ open: false });

    delete = () => {
        window.ipcRenderer.once('delete-collection', (event, result) => {
            this.setState({ error: !result, loading: false }); 
            if (this.state.error !== true) {
                this.close(); 
                this.props.refresh(); 
            } else {
                console.log(result);
            }
        }); 

        window.ipcRenderer.send('delete-collection', this.props.title); 
        console.log(this.props.title); 
        this.setState({ loading: true }); 
    }

    render() {
        return (
            <Modal trigger={ <Icon name="ellipsis horizontal" onClick={this.open}/> } basic open={this.state.open} >
                <Header icon='archive' size="huge" content={this.props.title} />
                <Modal.Content>
                    <TagMenu />
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='orange' inverted onClick={this.delete} disabled={this.state.loading}>
                        <Icon name="trash" /> Delete
                    </Button>
                    <Button basic color='red' inverted onClick={this.close} disabled={this.state.loading}>
                        <Icon name='remove' /> Cancel
                    </Button>
                    <Button color='green' inverted onClick={this.createEntry} disabled={this.state.loading}>
                        <Icon name='save outline' /> Save
                    </Button>
                </Modal.Actions>
            </Modal>
        ); 
    }
}

export default CollectionDetailModal; 