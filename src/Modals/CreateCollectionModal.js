import React, { Component } from 'react'; 
import { Modal, Icon, Button, Input, Header, Transition, Message } from 'semantic-ui-react'; 

class CreateCollectionModal extends Component {
    constructor(props) {
        super(props); 

        this.state = {
            newCollectionName: "", 
            open: false, 
            loading: false, 
            error: false
        }; 

        this.refresh = props.refresh;
    }

    componentDidMount = () => {

    }

    onNameChange = (event, { value }) => this.setState({ newCollectionName: value });

    close = () => this.setState({ open: false, loading: false }); 

    open = () => this.setState({ open: true, loading: false }); 

    createCollection = () => {
        window.ipcRenderer.once('create-collection', (event, result) => {
            this.setState({ error: !result, loading: false }); 
            if (this.state.error !== true) {
                this.close();
                this.refresh(); 
            }
        }); 

        if (this.state.newCollectionName !== "") {
            window.ipcRenderer.send('create-collection', this.state.newCollectionName); 
            this.setState({ loading: true });
        }
    }

    render() {
        return (
            <Modal trigger={<Button fluid basic primary content="New Collection" onClick={this.open}/>} basic size="small" 
                open={this.state.open} closeOnEscape={false} closeOnDimmerClick={false}>
                <Header icon="book" content="Create New Collection" />
                <Modal.Content>
                    <Input fluid placeholder="New Collection Name" onChange={this.onNameChange} loading={this.state.loading} error={this.state.error} />
                    <Transition visible={ this.state.error } animation="fade" duratiom={750}>
                        <Message floating error header="Invalid collection name!" content="Cannot be the same as an existing collection, or empty."/>
                    </Transition>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='red' inverted onClick={this.close} disabled={this.state.loading}>
                        <Icon name='remove' /> Cancel
                    </Button>
                    <Button color='green' inverted onClick={this.createCollection} disabled={this.state.loading}>
                        <Icon name='checkmark' /> Create
                    </Button>
                </Modal.Actions>
            </Modal>
        ); 
    }
}

export default CreateCollectionModal; 