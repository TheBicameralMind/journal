# Journal
This is my most recent side project, which is an attempt to build a journaling
application that lets me write using Markdown, and securely stores entries in a
format that makes sense to me. (If it becomes a serious attempt at anything,
I'll have to find a trendy tech name without vowels like *jrnl*). 

It's being built using React and Electron (mostly so I can learn them), with 
SQLite for storage, and I'm using 
[Semantic UI React](https://react.semantic-ui.com/) for components so I can
spend more time making it do what I want it to. 