const fs = require('fs'); 
const path = require("path");
const isDev = require("electron-is-dev"); 
const { BrowserWindow, app, ipcMain } = require('electron'); 

const { AuthInteractor, StorageInteractor } = require('./interactor'); 

let mainWindow;
let authInteractor; 
let storageInteractor; 
let encryptionPassword; 

let databasePath = path.join(isDev ? app.getAppPath() : app.getPath('userData'), 'database.db');

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 800, 
        height: 600, 
        icon: __dirname + '/logo512.png',
        webPreferences: {
            nodeIntegration: true, 
            preload: __dirname + '/preload.js'
        }
    });

    mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`); 

    mainWindow.maximize(); 

    app.setAboutPanelOptions({
        applicationName: "Journal", 
        applicationVersion: "0.0.1"
    }); 

    mainWindow.on('closed', () => mainWindow = null); 

    authInteractor = new AuthInteractor(databasePath); 
    storageInteractor = new StorageInteractor(databasePath);
}

app.on('ready', createWindow); 

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        authInteractor.setPassword(encryptionPassword); 
        app.quit(); 
    }
}); 

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow(); 
    }
}); 

ipcMain.handle('login-attempt', async (event, password) => {
    const { result, pass } = await authInteractor.checkPassword(password); 
    if (result) encryptionPassword = password; 
    return result;
});

ipcMain.on("set-password", (event, password) => {
    // authInteractor.setPassword(password); 

    encryptionPassword = password; 

    storageInteractor.init(isDev); 

    event.sender.send('set-password', true); 
}); 

ipcMain.on("should-login", (event) => {
    fs.stat(databasePath, (err, stats) => {
        if (err === null && stats.size > 0) event.sender.send('should-login', true); 
        else event.sender.send('should-login', false)
    })
}); 

ipcMain.on('get-collection-names-and-dates', (event) => {
    let names = storageInteractor.getSidebarCollectionData(); 
    event.sender.send('get-collection-names-and-dates', names); 
}); 

ipcMain.on('get-entries-for-collection', (event, title) => {
    let entries = storageInteractor.getEntriesForCollection(title); 
    event.sender.send('get-entries-for-collection', entries); 
}); 

ipcMain.on('get-content-for-entry', (event, id) => {
    let content = storageInteractor.getContentForEntry(id); 
    event.sender.send('get-content-for-entry', content); 
}); 

ipcMain.on('get-collections', (event) => {
    let names = storageInteractor.getCollections(); 
    event.sender.send('get-collections', names); 
}); 

ipcMain.on('update-entry', (event, entryData) => {
    storageInteractor.updateEntry(entryData); 
}); 

ipcMain.on('create-collection', (event, newName) => {
    let success = storageInteractor.addCollection(newName); 
    event.sender.send('create-collection', success); 
});

ipcMain.on('create-entry', (event, entryData) => {
    let success = storageInteractor.createEntry(entryData);
    event.sender.send('create-entry', success); 
}); 

ipcMain.on('delete-entry', (event, id) => {
    let success = storageInteractor.deleteEntry(id); 
    event.sender.send('delete-entry', success); 
}); 