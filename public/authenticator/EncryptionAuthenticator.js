const { Encryptor } = require('../encryption'); 

class EncryptionAuthenticator {
    constructor(storageLocation) {
        this.storageLocation = storageLocation;
        this.encryptor = new Encryptor(storageLocation); 
    }
    
    async checkPassword(password) {
        let result = await this.encryptor.decrypt(password); 
        return result; 
    }

    setPassword(password) {
        this.encryptor.encrypt(password); 
    }
}

module.exports = EncryptionAuthenticator;