const keytar = require('keytar'); 

const service = "Journal"; 
const username = "Default"

class KeychainAuthenticator {
    async checkPassword(password) {
        let correctPassword = await keytar.getPassword(service, username);
        return (correctPassword === password); 
    }

    setPassword(password) {
        keytar.setPassword(service, username, password).then(() => { }); 
    }
}

module.exports = KeychainAuthenticator; 