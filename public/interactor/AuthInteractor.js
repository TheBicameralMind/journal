const { KeychainAuthenticator, EncryptionAuthenticator } = require('../authenticator'); 

class AuthInteractor {
    constructor(path) {
        // this.authenticator = new KeychainAuthenticator(); 
        this.authenticator = new EncryptionAuthenticator(path); 
    }

    checkPassword(password) {
        return this.authenticator.checkPassword(password); 
    }

    setPassword(password) {
        this.authenticator.setPassword(password); 
    }
}

module.exports = AuthInteractor; 