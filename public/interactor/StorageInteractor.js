const { SqliteStorage } = require('../sqlite'); 

class StorageInteractor {
    constructor(storageLocation) {
        this.storage = new SqliteStorage(storageLocation); 
    }

    init(isDev) {
        this.storage.init(isDev); 
    }

    addCollection(collectionName) {
        return this.storage.addCollection(collectionName); 
    }

    deleteCollection(collectionName) {
        this.storage.deleteCollection(collectionName); 
    }

    getCollections() {
        return this.storage.getCollections(); 
    }

    getSidebarCollectionData() {
        return this.storage.getSidebarCollectionData(); 
    }

    getEntriesForCollection(collectionName) {
        return this.storage.getEntriesForCollection(collectionName); 
    }

    getContentForEntry(id) {
        return this.storage.getContentForEntry(id); 
    }

    updateEntry(entryData) {
        this.storage.updateEntry(entryData); 
    }

    createEntry(entryData) {
        return this.storage.createEntry(entryData); 
    }

    deleteEntry(id) {
        return this.storage.deleteEntry(id); 
    }
}

module.exports = StorageInteractor; 