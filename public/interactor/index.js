const AuthInteractor = require('./AuthInteractor'); 
const StorageInteractor = require('./StorageInteractor'); 

exports.AuthInteractor = AuthInteractor; 
exports.StorageInteractor = StorageInteractor; 