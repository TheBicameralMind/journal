CREATE TABLE Collections (
    collectionTitle VARCHAR(60) PRIMARY KEY
); 

CREATE TABLE Entries (
    entryId INTEGER PRIMARY KEY AUTOINCREMENT,
    entryDateCreated DATE NOT NULL,
    entryDateModified DATE NOT NULL, 
    entryTitle VARCHAR(100) NOT NULL, 
    entryContent TEXT NOT NULL DEFAULT '', 
    collectionTitle VARCHAR(60) NOT NULL, 
    
    FOREIGN KEY (collectionTitle) REFERENCES Collections (collectionTitle)
        ON UPDATE CASCADE ON DELETE CASCADE
); 

CREATE TABLE EntryTags (
	entryId INTEGER NOT NULL, 
	tag VARCHAR(50) NOT NULL, 
	PRIMARY KEY (entryId, tag), 
	
	FOREIGN KEY entryId REFERENCES Entries (entryId)
		ON UPDATE CASCADE ON DELETE CASCADE
); 

CREATE TABLE CollectionTags (
    collectionTitle VARCHAR(60) NOT NULL, 
    tag VARCHAR(50) NOT NULL, 
    PRIMARY KEY (collectionTitle, tag), 

    FOREIGN KEY collectionTitle REFERENCES Collections (collectionTitle)
        ON UPDATE CASCADE ON DELETE CASCADE
); 

-- CREATE TABLE Attachments (
--     entryId INT PRIMARY KEY, 
--     attachmentTitle VARCHAR(50), 
--     attachmentContent BLOB NOT NULL, 

-- 	UNIQUE(attachmentTitle), 
--     FOREIGN KEY (entryId) REFERENCES Entries (entryId)
--         ON UPDATE CASCADE ON DELETE CASCADE
-- ); 

