insert into Collections values 
    ("Test1"), 
    ("Test2"), 
    ("another test"), 
    ("this is test data"), 
    ("More Test Names"), 
    ("Test Further :)"); 

insert into Entries (entryDateCreated, entryDateModified, entryTitle, collectionTitle) values 
    ("02-05-2020", "02-07-2020", "A test entry", "Test1"),
    ("04-09-2020", "04-14-2020", "Another test", "Test1"),
    ("08-13-2020", "09-07-2020", "Real test hours", "Test1");

insert into Entries (entryDateCreated, entryDateModified, entryTitle, collectionTitle) values 
    ("03-24-2019", "02-01-2020", "I love testing!", "Test2"),
    ("04-14-2020", "04-14-2020", "haha fun stuff", "Test2"),
    ("08-23-2020", "09-07-2020", "Testing testing!", "another test");