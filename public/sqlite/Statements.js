let stmt = {
    collections: {
        getCollections: 'SELECT * FROM Collections', 
        getSidebarCollectionData: 'SELECT Collections.collectionTitle, max(Entries.entryDateCreated) AS "max", min(Entries.entryDateCreated) AS "min", count(Entries.entryId) AS "total" FROM Collections LEFT OUTER JOIN Entries ON Collections.collectionTitle = Entries.collectionTitle GROUP BY Collections.collectionTitle;', 
        addCollection: 'INSERT INTO Collections VALUES (?)', 
        deleteCollection: 'DELETE FROM Collections WHERE collectionTitle = ?',
        renameCollection: 'UPDATE Collections SET collectionTitle = @newName WHERE collectionTitle = @oldName'
    }, 
    entries: {
        getEntriesForCollection: 'SELECT collectionTitle as "collection", entryId AS "id", entryTitle AS "title", entryDateCreated AS "created", entryDateModified AS "modified" FROM Entries WHERE collectionTitle = ?',
        getContentForEntry: 'SELECT entryContent as "content", entryTitle as "title", entryDateCreated as "created", entryDateModified as "modified", collectionTitle as "collection" FROM Entries WHERE entryId = ?',
        updateEntry: 'UPDATE Entries SET entryTitle = @entry_title, entryContent = @entry_content, collectionTitle = @entry_collection, entryDateModified = @entry_modified WHERE entryId = @entry_id',
        createEntry: 'INSERT INTO Entries (entryTitle, collectionTitle, entryDateCreated, entryDateModified) VALUES (@entryTitle, @entryCollection, @entryCreated, @entryModified)',
        deleteEntry: 'DELETE FROM Entries WHERE entryId = ?'
    },

}; 

module.exports = stmt; 