const Database = require('better-sqlite3'); 
const fs = require('fs'); 
const path = require('path'); 
const sqlStatements = require('./Statements'); 

class SqliteStorage {
    constructor(dbPath) {
        this.dbPath = dbPath; 
        this.db = new Database(dbPath); 

        this.statements = {}; 
    }

    init(isDev) {
        let setup = fs.readFileSync(path.join(__dirname, 'journalDatabase.sql'), 'utf-8'); 
        this.db.exec(setup); 
        console.log(isDev);

        if (isDev) {
            let testData = fs.readFileSync(path.join(__dirname, 'test-data.sql'), 'utf-8');
            this.db.exec(testData); 
        }
    }
    
    getCollections() {
        if (!this.statements.hasOwnProperty('collections_list')) {
            this.statements.collections_list = this.db.prepare(sqlStatements.collections.getCollections);
        }
        return this.statements.collections_list.all(); 
    }

    getSidebarCollectionData() {
        if (!this.statements.hasOwnProperty('collections_dateRange')) {
            this.statements.collections_dateRange = this.db.prepare(sqlStatements.collections.getSidebarCollectionData);
        }
        return this.statements.collections_dateRange.all(); 
    }

    addCollection(collectionName) {
        if (!this.statements.hasOwnProperty('collections_add')) {
            this.statements.collections_add = this.db.prepare(sqlStatements.collections.addCollection);
        } 
        try {
            this.statements.collections_add.run(collectionName); 
            return true;
        } catch (err) {
            return false; 
        }
    }
    
    deleteCollection(collectionName) {
        if (!this.statements.hasOwnProperty('collections_delete')) {
            this.statements.collections_delete = this.db.prepare(sqlStatements.collections.deleteCollection);
        }
        this.statements.collections_delete.run(collectionName); 
    }

    renameCollection(oldCollectionName, newCollectionName) {
        if (!this.statements.hasOwnProperty('collections_rename')) {
            this.statements.collections_rename = this.db.prepare(sqlStatements.collections.renameCollection);
        }
        this.statements.collections_rename.run({
            newName: newCollectionName, 
            oldName: oldCollectionName
        }); 
    }

    getEntriesForCollection(collectionName) {
        if (!this.statements.hasOwnProperty("entry_list")) {
            this.statements.entry_list = this.db.prepare(sqlStatements.entries.getEntriesForCollection);
        }
        return this.statements.entry_list.all(collectionName); 
    }

    getContentForEntry(id) {
        if (!this.statements.hasOwnProperty("entry_content")) {
            this.statements.entry_content = this.db.prepare(sqlStatements.entries.getContentForEntry);
        }
        return this.statements.entry_content.get(id); 
    }

    updateEntry(entryData) {
        if (!this.statements.hasOwnProperty("entry_update")) {
            this.statements.entry_update = this.db.prepare(sqlStatements.entries.updateEntry);
        }
        this.statements.entry_update.run(entryData); 
    }

    createEntry(entryData) {
        if (!this.statements.hasOwnProperty("entry_create")) {
            this.statements.entry_create = this.db.prepare(sqlStatements.entries.createEntry); 
        }
        try {
            this.statements.entry_create.run(entryData); 
            return true;
        } catch (err) {
            return false; 
        }
    }

    deleteEntry(id) {
        if (!this.statements.hasOwnProperty('entry_delete')) {
            this.statements.entry_delete = this.db.prepare(sqlStatements.entries.deleteEntry); 
        }
        try {
            this.statements.entry_delete.run(id); 
            return true; 
        } catch (err) {
            console.log(err);
            return false; 
        }
    }
}

module.exports = SqliteStorage; 