const crypto = require('crypto');
const rw = require('rw-stream');  
const fs = require('fs'); 

class Encryptor {
    constructor(filePath) {
        this.algorithm = "aes-256-gcm"; 
        
        this.filePath = filePath; 
    }

    async encrypt(password) {
        if (this.password !== null && this.password !== "") {
            let { readStream, writeStream } = await rw(this.filePath); 
            let keyHashed = this.__toHashKey(password);
            let cipher = this.__cipher(keyHashed); 
            readStream.pipe(cipher).pipe(writeStream); 
        }
    }

    async decrypt(password) {
        try {
            let { readStream, writeStream } = await rw(this.filePath); 
            let keyHashed = this.__toHashKey(password);
            let cipher = this.__cipher(keyHashed); 
            readStream.pipe(cipher).pipe(writeStream); 
            
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    __cipher(keyHashed) {
        return crypto.createCipheriv(this.algorithm, keyHashed, __iv()); 
    }

    __toHashKey(plainKey) {
        return crypto.createHash('sha256')
                     .update(String(plainKey))
                     .digest('hex')
                     .substring(0, 32); 
    }

    __iv() {
        return crypto.randomBytes(16); 
    }
}

module.exports = Encryptor; 